<?php

namespace naknak987;

use Composer\Script\Event;
use Composer\Installer\PackageEvent;

class setup
{
    public static function postPackageInstall(PackageEvent $event)
    {
        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        
        file_put_contents($vendorDir . '/test.env', "testing", FILE_APPEND);
    }
}